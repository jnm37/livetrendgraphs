/*
Constructor for trend graph object
draws the chart (in a square with sidelength s) on Raphael object paper
numSamples should be an integer representing how far back the trend graph records
inputGetters should be an array of function handles, each of which has no arguments and returns a number
*/
function TrendGraph(paper, cornerX, cornerY, height, numSamples, inputGetters, rangeMins, rangeMaxes, labels, noSignalValue) {
	var numSeries = rangeMins.length;
	var yvalues = new Array(numSeries);
	for (var j = 0; j<numSeries; j++) {
		yvalues[j] = new Array(numSamples);
	}
	this.lineColors = new Array(numSeries); //Array of strings representing colors as per Raphael specifications
	this.now = 0;
	var xinc = Math.round(height/numSamples);
	
	
	//Initialize graph data to 0 except at present
	if (noSignalValue == undefined) {
		noSignalValue = 0;
	}
	for (var i = 1; i<numSamples; i++) {
		for (var j = 0; j<numSeries; j++) {
			yvalues[j][i] = noSignalValue;
		}
	}
	for (var j = 0; j<numSeries; j++) {
		yvalues[j][0] = normalizeInRange(inputGetters[j](), rangeMaxes[j], rangeMins[j]);
	}
	
	//Assign Colors
	this.lineColors[0] = "blue";
	if (numSeries > 1) {
		this.lineColors[1] = "red";
	}
	if (numSeries > 2) {
		this.lineColors[2] = "green";
	}
	if (numSeries > 3) {
		this.lineColors[3] = "black";
	}
	for (var j = 4; j < numSeries; j++) {
		var redpart = Math.floor(256*Math.random());//the rest of the colors are randomly generated
		var bluepart = Math.floor(256*Math.random());
		var greenpart = Math.floor(256*Math.random());
		this.lineColors[j] = "rgb("+redpart+","+greenpart+","+bluepart+")";
	}
	
	//Draw Graph
	this.borderBox = paper.rect(cornerX-20, cornerY-20, height+100, height+40, 10).attr({'stroke': '#a81'});//box around the graph (buffer of 15px)
	paper.path("M "+(cornerX+height)+" "+(cornerY+height)+" v "+(-height)).attr({'arrow-end': 'classic-wide-long'});//y-axis
	paper.path("M "+(cornerX+height)+" "+(cornerY+height)+" h"+(-height)).attr({'arrow-end': 'classic-wide-long'});//x-axis
	paper.text(cornerX+height+15, cornerY, "100%");
	paper.text(cornerX+height+10, cornerY+height, "0%");
	paper.text(cornerX+height, cornerY+height+10, "0");
	paper.text(cornerX, cornerY+height+10, numSamples.toString());
	//TODO: gridlines?
	var lines = new Array(numSeries);//Array of color-coded path elements making up the lines on the graph
	for (var j = 0; j<numSeries; j++) {
		lines[j] = new Array(numSamples);
		for (var i = 1; i<numSamples; i++) {
			lines[j][i] = paper.path("M "+Math.round(cornerX+height-i*height/numSamples)+" "+Math.round(cornerY+(1-noSignalValue)*height)+" L "+Math.round(cornerX+height-(i+1)*height/numSamples)+" "+Math.round(cornerY+(1-noSignalValue)*height)).attr({'stroke': this.lineColors[j]});
		}
		lines[j][0] = paper.path("M "+Math.round(cornerX+height)+" "+Math.round(cornerY+(1-yvalues[j][0])*height)+" L "+Math.round(cornerX+height-height/numSamples)+" "+Math.round(cornerY+(1-noSignalValue)*height)).attr({'stroke': this.lineColors[j]});
	}
	var lineVisible = new Array(numSeries); //Array of booleans representing whether each line is visible
	var legend = new Array(numSeries); //Array of color-coded text elements making up the legend
	function addLineHider(whichLine) {
		legend[whichLine].click(function() {
			if (lineVisible[whichLine]) {
				for (var i=0; i<numSamples; i++) {
					lines[whichLine][i].hide();
				}
			} else {
				for (var i=0; i<numSamples; i++) {
					lines[whichLine][i].show();
				}
			}
			lineVisible[whichLine] = !lineVisible[whichLine];
		});
	}
	for (var j = 0; j<numSeries; j++) {
		legend[j] = paper.text(cornerX+height+10, cornerY+20+15*j, labels[j]+" ("+rangeMins[j]+"-"+rangeMaxes[j]+")").attr({'text-anchor': 'start', 'fill': this.lineColors[j]});
		lineVisible[j] = true;
		addLineHider(j);//I can't just use anonymous functions to toggle visiibility because the scope of j would be wrong
	}
	
	//Update Graph
	this.tick = function() {
		this.now = (this.now-1+numSamples)%numSamples;
		for (var j = 0; j<numSeries; j++) {
			yvalues[j][this.now] = normalizeInRange(inputGetters[j](), rangeMaxes[j], rangeMins[j]);
			for (var i = 0; i<numSamples; i++) {
				lines[j][i].translate(-xinc, 0);
			}
			lines[j][this.now].remove();
			lines[j][this.now] = paper.path("M "+(cornerX+height)+" "+Math.round(cornerY+(1-yvalues[j][this.now])*height)+" L "+(cornerX+height-xinc)+" "+Math.round(cornerY+(1-yvalues[j][(this.now+1)%numSamples])*height)).attr({'stroke': this.lineColors[j]});
			if (!lineVisible[j]) {
				lines[j][this.now].hide();
			}
		}
	}
	
	//Change Color Scheme
	this.updateColors = function(newColors) {
		this.lineColors = newColors;
		for (var j=0; j<numSeries; j++) {
			legend[j].attr({'fill': newColors[j]});
			for (var i=0; i<numSamples-1; i++) {
				lines[j][i].attr({'stroke': newColors[j]});
			}
		}
	}
	
	//TODO: allow changes to line dashes and symbols at points
	
	//Reset the graph so that the history is 0. Be sure to reset what the inputGetters read first
	this.startState = function() {
		this.now = 0;
		for (var j=0; j<numSeries; j++) {
			lineVisible[j] = true;
			for (var i=1; i<numSamples; i++) {
				yvalues[j][i] = noSignalValue;
			}
			yvalues[j][0] = normalizeInRange(inputGetters[j](), rangeMaxes[j], rangeMins[j]);
			for (var i=1; i<numSamples; i++) {
				lines[j][i].remove();
				lines[j][i] = paper.path("M "+Math.round(cornerX+height-i*height/numSamples)+" "+Math.round(cornerY+(1-noSignalValue)*height)+" L "+Math.round(cornerX+height-(i+1)*height/numSamples)+" "+Math.round(cornerY+(1-noSignalValue)*height)).attr({'stroke': this.lineColors[j]});
			}
			lines[j][0].remove();
			lines[j][0] = paper.path("M "+Math.round(cornerX+height)+" "+Math.round(cornerY+(1-yvalues[j][0])*height)+" L "+Math.round(cornerX+height-height/numSamples)+" "+Math.round(cornerY+(1-noSignalValue)*height)).attr({'stroke': this.lineColors[j]});
		}
		
	}
}

function normalizeInRange(input, max, min) {
	if (input > max) {
		return 1;
	} else if (input < min) {
		return 0;
	} else {
		return (input-min)/(max-min);
	}
}