# LiveTrendGraphs

##Purpose
LiveTrends.js allows the user to put an easily updated line graph inside a Raphael svg element.

##Parameters

	new LiveTrend(paper, cornerX, cornerY, height, numSamples, inputGetters, rangeMins, rangeMaxes, labels, noSignalValue);

###paper
Raphael object on which the graph is drawn

###cornerX
The x-value of the point on the top left corner of the graph

###cornerY
The y-value of the point on the top left corner of the graph

###height
The length in pixels of the axes

###numSamples
The numer of line segments in each series--points labeled from present to numSamples ticks ago are represented

###inputGetters
An array of function handlers which read the next y-values

###rangeMins
An array of numbers representing the values for each series which would correspond to a point landing on the x-axis

###rangeMaxes
An array of numbers representing the values for each series which would correspond to a point landing at the top of the graph

###labels
An array of strings representing the names of each series

###noSignalValue
An optional number between 0 and 1 determining where on the graph uninitialized points fall

##Properties

###lineColors
An array of strings representing the colors of the lines--use updateColors() to change the colors

###borderBox
Raphael rect object for border drawn around the graph

###tick()
The function which causes the graph to read the next input data and update itself

###updateColors(newColors)
The function used to change the colors of the lines

####newColors
An array of strings, each of which can be parsed as colors by Raphael--see http://dmitrybaranovskiy.github.io/raphael/reference.html#Element.attr for more information

###startState()
The function which resets the graph, including visibility of lines

##Sample Code

	<style type="text/css">
		#canvas_container{
			width: 500px;
			border: 1px solid #aaa;
		}
	</style>
	<script type="text/javascript" src="raphael.min.js"></script>
	<script type="text/javascript" src="LiveTrend.js"></script>
	<script type="text/javascript">
		var liveGraph;
		window.onload = function() {
			var paper = new Raphael(document.getElementById('canvas_container'), 500, 500);
			var addPoint1 = function() {
				return Math.random()*10;
			}
			var addPoint2 = function() {
				return Math.random()*5;
			}
			liveGraph = new TrendGraph(100, 100, 50, 10, [addPoint1, addPoint2], [0,0], [0,0], ["Random 1", "Random 2"], 0.5);
		}
		function beginFlow() {
			window.setInterval(function() {
				liveGraph.tick();
			});
		}
	</script>
	<div id="canvas_container"></div>
	<button type="button" onclick="beginFlow()">Start</button>